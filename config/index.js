/**
 * @file 系统常量配置
 * @author WuFeng
 * @copyright 百旺祥云
 * @createDate 2019-08-27 15:53:00
 */

// const isServer = process.server
// const isClient = process.client

/**
 * @const {String}
 * @desc 获取 location 或 URL 的 hostname 和 port 号码
 */
// export const ENV_HOST = window.location.host

/**
 * @const {String}
 * @desc 获取 location 或 URL 的主机名称部分
 */
// export const ENV_HOSTNAME = window.location.hostname

/**
 * @const {String}
 * @desc token名称
 */
export const TOKEN_KEY = 'token'

/**
 * @enum {Object}
 * @desc 短信验证码生命周期
 */
export const SMS_CODE_TIMER = 120
