## 构建流程

``` bash
# 安装依赖
$ npm install

# 在本地主机上使用热重载： localhost:3000
$ npm run dev

# 为生产和启动服务器生成
$ npm run build
$ npm run start

# 生成静态项目
$ npm run generate
```

有关工作原理的详细说明，请查看 [Nuxt.js docs](https://nuxtjs.org).


# 一. 部署应用到服务器
参考：
	https://segmentfault.com/a/1190000014450967
	https://segmentfault.com/a/1190000012774650

### 搭建nginx+node+npm+pm2环境

## 1.安装node,pm2

``` bash
#安装pm2
$ npm install -g pm2
```

## 2.Nginx配置
```nginx
upstream nodenuxt {
    server 127.0.0.1:3002; #nuxt项目 监听端口
    keepalive 64;
}

server {
    listen 80;
    server_name mysite.com;
    location / {
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;  
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_set_header X-Nginx-Proxy true;
        proxy_cache_bypass $http_upgrade;
        proxy_pass http://nodenuxt; #反向代理
    }
}
```

## 3.npm run build 打包应用
打包完成后，我们将
```
.nuxt
static
nuxt.config.js
package.json
```

## 4.在服务器上部署运行
> 1.运行npm install 安装package里的依赖
> 2.运行npm start 就可以运行起来nuxt的服务渲染了
> 此时我们就可以在浏览器上输入 mysite.com 访问了。服务端渲染瞬间出来了，但这并不理想，进程稳定性能否常驻后台？

## 5.pm2开启进程守护
进入对应的应用目录，执行以下命令
```bash
$ pm2 start --name xyqf-web ./node_modules/nuxt/bin/nuxt.js -- start
```
其中 xyqf-web 我们在package中的项目名称。

# 二. 运维jenkins自动化部署
模拟场景：项目访问网址[www.mysite.com]，nuxt服务端口：3000
``` nginx
# 准备工作：配置Nginx，安装node，安装pm2，安装npm（实际中考虑npm传输慢使用cnpm）

## Nginx配置
upstream nodenuxt {
    server 127.0.0.1:3000; #nuxt项目 监听端口
    keepalive 64;
}

server {
    listen 80;
    server_name www.mysite.com;
    location / {
        proxy_read_timeout 60;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;  
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_set_header X-Nginx-Proxy true;
        proxy_cache_bypass $http_upgrade;
        proxy_pass http://nodenuxt; #反向代理
    }
}
```

``` bash
# 1.拉取master分支代码至jenkins工作空间目录

# 2.在jenkins工作空间运行npm install 安装package里的依赖
$ npm install

# 3.在jenkins工作空间打包应用
$ npm run build:test # 测试环境
$ npm run build:prod # 生产环境

# 4.打包完成后，我们将以下四个移动至项目部署目录
.nuxt
static
nuxt.config.js
package.json

# 5.在项目部署目录运行npm install 安装package里的依赖
$ npm install

# 6.启动项目：使用pm2开启进程守护
$ pid=`ps -ef | grep nuxt.js | grep -v grep | awk '{print $2}'`
if [ -n "$pid" ]
then
   kill -9 $pid
fi
$ pm2 start --name xyqf-web ./node_modules/nuxt/bin/nuxt.js -- start
 --max_memory_restart 1024M

# 压测
$ http_load -p 10 -s 60 -r 3 -f 200 ./yctest.txt
$ http_load -p 999 -s 1 -r 999 -f 999 ./yctest.txt
$ http_load -p 2000 -s 1 -r 2000 -f 2000 ./yctest.txt

#### $ pm2 start ./bin/www —name guide -i 1 —max-memory-restart 400M
```
