/**
 * Created by wufeng on 2018/08/15.
 */

export function isvalidUsername(str) {
  // const valid_map = ['admin', 'editor']
  // return valid_map.indexOf(str.trim()) >= 0
  if (str.length === 0) {
    return 0
  } else {
    return 1
  }
}

/*
 * @description 校验密码
    要求一、
    1.密码必须由字母、数字、特殊符号组成，区分大小写
    2.特殊符号包含（. _ ~ ! @ # $ ^ & *）
    3.密码长度为8-20位

    实现1：
    ^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[._~!@#$^&*])[A-Za-z0-9._~!@#$^&*]{8,20}$
    实现2：
    ^(?![a-zA-Z]+$)(?![0-9]+$)(?![._~!@#$^&*]+$)(?![0-9a-zA-Z]+$)(?![0-9._~!@#$^&*]+$)(?![a-zA-Z._~!@#$^&*]+$)[A-Za-z0-9._~!@#$^&*]{8,20}$

    要求二、
    1.密码必须由字母、数字组成，区分大小写
    2.密码长度为8-18位

    实现1：
    ^(?=.*[a-zA-Z])(?=.*[0-9])[A-Za-z0-9]{8,18}$
    实现2：
    ^(?![a-zA-Z]+$)(?![0-9]+$)[A-Za-z0-9]{8,18}$
    ————————————————
    版权声明：本文为CSDN博主「wantingtingting」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。
    原文链接：https://blog.csdn.net/wantingtingting/article/details/87938299
  */
export function validatePassword(str, type = '') {
  // ^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9]).{6,})\S$ // 长度6位以上，至少包含1个数字，1个大写字母，1个小写字母，不包含空格
  const reg = /^[\da-zA-Z]{6,18}$/
  return (type === 'get') ? reg : reg.test(str)
  // return true
}

/* 合法uri*/
export function validateURL(textval, type = '') {
  const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return (type === 'get') ? urlregex : urlregex.test(textval)
}

/* 小写字母*/
export function validateLowerCase(str, type = '') {
  const reg = /^[a-z]+$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 大写字母*/
export function validateUpperCase(str, type = '') {
  const reg = /^[A-Z]+$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 大小写字母*/
export function validateAlphabets(str, type = '') {
  const reg = /^[A-Za-z]+$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 数字和26个英文字母组成的字符串 */
export function validateAlphabetsNumber(str, type = '') {
  const reg = /^[A-Za-z0-9]+$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 数字和26个英文字母且字母打头组成的字符串 */
export function validateVariableName(str, type = '') {
  const reg = /^[a-zA-z]+[A-Za-z0-9]*$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 整数 */
export function validateInteger(str, type = '') {
  const reg = /^-?\d+$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 正整数 */
export function validatePosInteger(str, type = '') {
  const reg = /^^[+]{0,1}(\d+)$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 汉字 */
export function validateChinese(str, type = '') {
  const reg = /^[\u4e00-\u9fa5],{0,}$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 身份证号码 */
export function validateIdCard(str, type = '') {
  const reg = /^\d{15}|\d{}18$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 邮箱 */
export function validateEmail(str, type = '') {
  // /^[\w][\w\d_]*@\w+\.[A-Za-z]+$/
  const reg = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 手机号 */
export function validateTelephone(str, type = '') {
  const reg = /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 邮编 */
export function validateZipCode(str, type = '') {
  const reg = /^[0-9]{6}$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 传真/座机 */
export function validateFax(str, type = '') {
  const reg = /^(\d{3,4}-)?\d{7,8}$/
  return (type === 'get') ? reg : reg.test(str)
}

/* 匹配win10文件名中不允许的字符 */
export function validateWindowsFilename(str, type = '') {
  // const pattern = /[\\\\/:\\*\\?"< >\\|'\\.]/g
  // str = str.replace(pattern, '_')
  const reg = /[\\\\/:\\*\\?"< >\\|'\\.]/g
  return (type === 'get') ? reg : reg.test(str)
}

/* 自定义正则校验 */
export function validateRegexp(reg = '', str) {
  return reg.test(str)
}
