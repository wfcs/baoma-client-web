import Cookies from 'js-cookie'
import { TOKEN_KEY } from '@/config'

//获取服务端cookie
export function getcookiesInServer(req) {
  const service_cookie = {}
  req && req.headers.cookie && req.headers.cookie.split(';').forEach(function(val) {
    const parts = val.split('=')
    service_cookie[parts[0].trim()] = (parts[1] || '').trim()
  })
  return service_cookie
}

//获取客户端cookie
export function getcookiesInClient(app, key) {
  // return Cookies.get(key) ? Cookies.get(key) : ''
}

export function setToken(app = null, token) {
  return app.$cookies.set(TOKEN_KEY, token, {
    path: '/'
    // domain: process.env.CURRENT_PLATFORM_INFO.mainDomain
  })
}

export function getToken(app = null) {
  if (app !== null) {
    return app.$cookies.get(TOKEN_KEY)
  } else {
    if (process.client) {
      return Cookies.get(TOKEN_KEY) || ''
    } else {
      return ''
    }
  }
}

export function removeToken(app) {
  app.$cookies.remove(TOKEN_KEY, {
    path: '/'
    // domain: process.env.CURRENT_PLATFORM_INFO.mainDomain
  })
}
