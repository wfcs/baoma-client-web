/**
 * @description 初始化路由信息
 */

// 需做301跳转的链接
const PATH_301_ENUM = ['/openlogin', '/register']
const PATH_301_TARGET_ENUM = ['/user/login', '/user/register']

export default function({ route, req, res, redirect, store }) {
  const routePath = route.path
  const pathIndex = PATH_301_ENUM.indexOf(routePath)
  if (pathIndex !== -1) {
    redirect(301, PATH_301_TARGET_ENUM[pathIndex])
  }
}
