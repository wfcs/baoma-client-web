const { resolve } = require('path')
require('dotenv').config({
  path: './.env.config'
})

let BASE_API = '' // 接口地址
let BASE_API_LAN = '' // 接口地址 - 内网

switch (process.env.BASE) {
  case 'dev':
    BASE_API = process.env.DEV_BASE_API
    BASE_API_LAN = process.env.DEV_BASE_API_LAN
    break
  case 'test':
    BASE_API = process.env.TEST_BASE_API
    BASE_API_LAN = process.env.TEST_BASE_API_LAN
    break
  case 'production':
    BASE_API = process.env.PROD_BASE_API
    BASE_API_LAN = process.env.PROD_BASE_API_LAN
    break
  default:
    BASE_API = process.env.PROD_BASE_API
    BASE_API_LAN = process.env.PROD_BASE_API_LAN
    break
}

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: `%s孕育宝`,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: `` },
      { hid: 'keywords', name: 'keywords', content: '财务软件,会计软件,记账软件,做账软件,税务软件' },
      { name: 'apple-mobile-web-app-capable', content: 'yes' }, // iOS浏览器禁止缩放
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, shrink-to-fit=no, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no' },
      { name: 'renderer', content: 'webkit' }, // 强制让360浏览器使用Webkit内核
      { 'http-equiv': 'X-UA-Compatible', content: 'IE=edge' }
    ],
    script: [],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: `/favicon.ico` }
    ]
  },
  // render: {
  //   resourceHints: false // 添加prefetch和preload，以加快初始页面加载时间。
  //   // etag: false,
  //   // static: {
  //   //   etag: false
  //   // }
  // },
  /*
  ** Customize the progress-bar color
  */
  loading: false,
  // loading: {
  //   color: '#fff',
  //   failedColor: '#f98e3e',
  //   duration: 1500
  // },
  /*
  ** Global CSS
  */
  css: [
    // 第三方插件 CSS 文件
    { src: 'element-ui/lib/theme-chalk/index.css' },
    // 自定义公共 CSS 文件
    '@/assets/styles/index.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/icon', // 字体图标
    '@/plugins/filters', // 通用过滤器
    { src: '@/plugins/element-ui', ssr: true } // element-ui
    // { src: '@/plugins/ImageUpload', ssr: false }, // 图片上传组件，可自定义裁切图片
    // { src: '@/plugins/vue-baidu-map', ssr: false }, // 百度地图组件 - 在ie下包含ie11运行报错
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // ['@nuxtjs/dotenv', {
    //   filename: '.env.config'
    // }]
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    ['cookie-universal-nuxt', { parseJSON: false }]
  ],
  serverMiddleware: [
  ],
  router: {
    middleware: [
      'initRouter' // 初始化路由信息
    ],
    extendRoutes(routes, resolve) {
      // routes.push(
      //   {
      //     name: 'custom',
      //     path: '*',
      //     component: resolve(__dirname, 'layouts/error.vue')
      //   }
      // )
    }
  },
  /*
  ** Build configuration
  */
  build: {
    // analyze: true, // 分析并可视化构建后的打包文件
    // assetFilter: function(assetFilename) {
    //   return assetFilename.endsWith('.js')
    // },
    // vendor: ['element-ui'],
    // productionSourceMap: false,
    // productionGzip: true,
    // productionGzipExtensions: ['js', 'css', 'svg'],
    // transpile: [],
    extractCSS: true, // 将主块中的 CSS 提取到一个单独的 CSS 文件中（自动注入模板），该文件允许单独缓存文件。
    // 优化css
    optimizeCSS: {
      assetNameRegExp: /\.optimize\.css$/g,
      cssProcessor: require('cssnano'),
      cssProcessorPluginOptions: {
        preset: ['default', { discardComments: { removeAll: true }}]
      },
      canPrint: true
    },
    babel: {
      plugins: [
        [
          'component',
          {
            'libraryName': 'element-ui',
            'styleLibraryName': 'theme-chalk'
          }
        ]
      ]
    },
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      if (ctx.isServer) {
        // 初始化/static/robots.txt
        const fs = require('fs')
        switch (process.env.BASE) {
          case 'production':
            fs.readFile('./config/robots/robotsProd.txt', 'utf-8', function(error, data) {
              //  用error来判断文件是否读取成功
              if (error) return console.log('读取文件./config/robots/robotsProd.txt失败,内容是' + error.message)
              fs.writeFile('./static/robots.txt', data, { flag: 'w', encoding: 'utf-8', mode: '0666' }, function(err) {
                if (err) {
                  console.log('文件./static/robots.txt写入失败')
                } else {
                  console.log('文件./static/robots.txt写入成功')
                }
              })
            })
            break
          case 'test':
            fs.readFile('./config/robots/robotsTest.txt', 'utf-8', function(error, data) {
              //  用error来判断文件是否读取成功
              if (error) return console.log('读取文件./config/robots/robotsTest.txt失败,内容是' + error.message)
              fs.writeFile('./static/robots.txt', data, { flag: 'w', encoding: 'utf-8', mode: '0666' }, function(err) {
                if (err) {
                  console.log('文件./static/robots.txt写入失败')
                } else {
                  console.log('文件./static/robots.txt写入成功')
                }
              })
            })
            break
          default:
            break
        }
      }

      // 排除 nuxt 原配置的影响,Nuxt 默认有vue-loader,会处理svg,img等
      // 找到匹配.svg的规则,然后将存放svg文件的目录排除
      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'))
      svgRule.exclude = [resolve(__dirname, 'assets/icons/svg')]

      // Includes /assets/icons/svg for svg-sprite-loader
      config.module.rules.push({
        test: /\.svg$/,
        include: [resolve(__dirname, 'assets/icons/svg')],
        loader: 'svg-sprite-loader',
        options: {
          symbolId: 'icon-[name]'
        }
      })

      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  env: {
    BASE_API,
    BASE_API_LAN
  },
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0' // default: localhost,
  }
}
