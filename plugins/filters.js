import Vue from 'vue'
import { parseTime, formatTime } from '@/utils/index'

const filters = {
  parseTime,
  formatTime
}

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
