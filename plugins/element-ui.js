import Vue from 'vue'
import Element from 'element-ui'
import locale from 'element-ui/lib/locale/lang/zh-CN'
Vue.use(Element, { locale })

// import {
//   Input,
//   Icon,
//   Button,
//   Radio,
//   Checkbox,
//   Select,
//   Cascader,
//   Form,
//   FormItem,
//   TabPane,
//   Tabs,
//   Loading
// } from 'element-ui'

// Vue.use(Input)
// Vue.use(Icon)
// Vue.use(Button)
// Vue.use(Radio)
// Vue.use(Checkbox)
// Vue.use(Select)
// Vue.use(Cascader)
// Vue.use(Form)
// Vue.use(FormItem)
// Vue.use(TabPane)
// Vue.use(Tabs)

// Vue.use(Loading.directive)
// Vue.prototype.$loading = Loading.service

