import Vue from 'vue'
import BaiduMap from 'vue-baidu-map/components/map/Map.vue'
import { BmNavigation, BmMarker, BmInfoWindow, BmScale } from 'vue-baidu-map'

Vue.component('baidu-map', BaiduMap)
Vue.component('bm-navigation', BmNavigation)
Vue.component('bm-marker', BmMarker)
Vue.component('bm-info-window', BmInfoWindow)
Vue.component('bm-scale', BmScale)

