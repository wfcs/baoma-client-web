import request from '~/utils/request'

/**
 * @description 获取用户会话key
 */
export function getPtk(params) {
  const data = {}
  return request({
    url: `v2/ptk`,
    method: 'get',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 邀请链接
 * @param {Object} params
 * @param {String} pid 推广码
 * @param {String} sid 产品id
 * @param {String} phone 手机号码
 */
export function platformPromotion(params) {
  const data = {
    pid: params.pid,
    sid: params.sid,
    phone: params.phone
  }
  return request({
    url: 'v2/from/platform',
    method: 'get',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 注册
 * @param {Object} params
 * @param {String} phoneNumber 手机号
 * @param {String} password 密码
 * @param {String} userName 昵称
 * @param {String} regionCode 区域代码
 * @param {String} logId 邀请记录ID
 * @param {String} pid 推广码
 * @param {String} sid 产品id
 * @param {String} code 短信验证码
 */
export function register(params) {
  const data = {
    phoneNumber: params.phoneNumber,
    password: params.password,
    userName: params.userName,
    regionCode: params.regionCode
  }
  return request({
    url: `v2/register?code=${params.smsCheckCode}&logId=${params.logId}&pid=${params.pid}&sid=${params.sid}`,
    method: 'post',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 发送手机验证码（注册）
 * @param {String} phoneNumber 用户名(手机号)
 */
export function sendSmsCode(params) {
  const data = {}
  return request({
    url: `v2/sendVerifyCode/${params.phoneNumber}`,
    method: 'get',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 注册关联用户企业
 * @param {Object} params
 * @param {String} companyName 企业名称
 * @param {String} dutyNumber 企业税号
 * @param {String} introduction 企业简介
 * @param {String} reg_tk 注册会话key
 */
export function regitrelCompany(params) {
  const data = {
    companyName: params.companyName,
    dutyNumber: params.dutyNumber,
    introduction: params.introduction
  }
  return request({
    url: `v2/regitrelCompany?reg_tk=${params.regTk}`,
    method: 'post',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 获取用户会话key
 */
export function getLgpkey(params) {
  const data = {}
  return request({
    url: 'v2/lgpkey',
    method: 'post',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 登录
 * @param {String} phoneNumber 手机号码
 * @param {String} password 加密后的密码
 * @param {String} pid 产品id
 * @param {String} pky 产品秘钥
 * @param {String} auth_tk 防止请求被拦截，放url
 * @param {String} pbky 用于RSA加密用户密码
 */
export function login(params) {
  const data = {
    phoneNumber: params.phoneNumber,
    password: params.password
  }
  return request({
    url: `v2/login?auth_tk=${params.auth_tk}&pid=${params.pid || ''}&pky=${params.pky}`,
    method: 'post',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 退出登录
 */
export function logout() {
  return request({
    url: 'v2/logout',
    method: 'get'
  })
}

/**
 * @description 发送验证码（忘记密码）
 * @param {String} phoneNumber 用户名(手机号)
 */
export function forgetPwdSendSmsCode(params) {
  const data = {}
  return request({
    url: `v2/sendResetCode/${params.phoneNumber}`,
    method: 'get',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 忘记密码-验证账号
 * @param {String} phoneNumber 用户名(手机号)
 * @param {String} verificationCode 短信验证码
 */
export function resertAccountCheck(params) {
  const data = {}
  return request({
    url: `v2/validateResetPwsCode/${params.verificationCode}/${params.phoneNumber}`,
    method: 'get',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 忘记密码-设置密码
 * @param {String} password 密码
 * @param {String} token 验证秘钥
 */
export function resertAccountPass(params) {
  const data = {
    password: params.password
  }
  return request({
    url: `v2/resetpwd?token=${params.token}`,
    method: 'post',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 获取登录人信息
 * @param {String} bwtk header或cookie或url上
 */
export function getProfileLive(params) {
  const data = {}
  return request({
    url: `v2/profile/live`,
    method: 'get',
    headers: {
      'Error-Echo': false
    },
    data
  })
}
