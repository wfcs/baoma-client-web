import request from '~/utils/request'

/**
 * @description 根据上级栏目ID获取下级栏目
 * @param {Object} params
 * {
      "params": {
        "typePid": 1
      }
    }
 */
export function getSonType(params) {
  const data = {
    params: {
      typePid: params.typePid
    }
  }
  return request({
    url: 'article/sonType',
    method: 'post',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 获取文章列表
 * @param {Object} params
 * {
    "page": {
      "size": 10,
      "num": 1
    },
    "params": {
      "typeId": 3,
      "typePid": 2
    }
  }
 */
export function getList(params) {
  const data = {
    page: {
      size: params.page.size,
      num: params.page.num
    },
    params: {
      typeId: params.params.typeId,
      typePid: params.params.typePid
    }
  }
  return request({
    url: 'article/list',
    method: 'post',
    headers: {
      'Error-Echo': false
    },
    data
  })
}

/**
 * @description 获取文章详情
 * @param {Object} params
 * {
    "params": {
      "id": 3
    }
  }
 */
export function getDetail(params) {
  const data = {
    params: {
      id: params.id
    }
  }
  return request({
    url: 'article/item',
    method: 'post',
    headers: {
      'Error-Echo': false
    },
    data
  })
}
