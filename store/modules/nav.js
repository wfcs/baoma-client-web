import { getSonType } from '@/api/article'

const state = ({
  navLst: {} // 登陆人信息
})

const getters = {
  navLst: state => state.navLst
}

const mutations = {
  SET_NAVLST: (state, navLst) => {
    state.navLst = navLst
  }
}

const actions = {
  // 初始化导航
  InitNavLst({ commit, state }) {
    return new Promise((resolve, reject) => {
      getSonType({
        typePid: 0
      }).then((response) => {
        const res = response.data.lstItem.reverse()
        commit('SET_NAVLST', res)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
