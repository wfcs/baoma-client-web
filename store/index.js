import Vue from 'vue'
import Vuex from 'vuex'
// import { getToken, removeToken } from '@/utils/auth'
import user from './modules/user'
import nav from './modules/nav'

Vue.use(Vuex)

const store = () => new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
    // ssr
    async nuxtServerInit({ commit, dispatch, state }, { app, req }) {
      // 初始化navLst
      await dispatch('nav/InitNavLst', {})
    //   // 如果token不为空
    //   if (getToken(app)) {
    //     await dispatch('user/SetToken', {
    //       token: getToken(app)
    //     })
    //     // 获取用户信息
    //     await dispatch('user/GetProfileLive', {}).then(response => {
    //     }).catch(err => {
    //       // 获取失败清除token
    //       removeToken(app)
    //       // console.log('---GetProfileLive--error')
    //       // console.log(err)
    //       err
    //     })
    //   }
    }
  },
  modules: {
    user,
    nav
  }
})

export default store
